The DA- Registered Farm Laborers is a fundamental dataset uploaded to the Philippine Geoportal System
(www.geoportal.gov.ph). The dataset provide a male gender-based statistics on registered farm laborers nationwide. The dataset was shared to the portal by the Department of Agriculture.

